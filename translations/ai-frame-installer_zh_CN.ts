<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>KyWindow</name>
    <message>
        <location filename="../src/kywindow.cpp" line="58"/>
        <source>Install successfully, restart to apply the opreation?</source>
        <translation>安装成功，是否重启以应用操作？</translation>
    </message>
    <message>
        <location filename="../src/kywindow.cpp" line="62"/>
        <source>Restart</source>
        <translation>重启</translation>
    </message>
    <message>
        <location filename="../src/kywindow.cpp" line="66"/>
        <source>Restart Later</source>
        <translation>稍后重启</translation>
    </message>
    <message>
        <location filename="../src/kywindow.cpp" line="90"/>
        <source>Install failed, please reselect and retry.</source>
        <translation>安装失败，请尝试重新选择后安装。</translation>
    </message>
    <message>
        <location filename="../src/kywindow.cpp" line="93"/>
        <location filename="../src/kywindow.cpp" line="146"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../src/kywindow.cpp" line="110"/>
        <source>Framework is installing, are you sure you want to quit?</source>
        <translation>框架正在安装中，是否确定要退出？</translation>
    </message>
    <message>
        <location filename="../src/kywindow.cpp" line="118"/>
        <source>Quit Now</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../src/kywindow.cpp" line="143"/>
        <source>KVM and package docker is not supported, please select other package.</source>
        <translation>系统不支持KVM，安装方式docker不支持，请选择其他安装方式。</translation>
    </message>
    <message>
        <location filename="../src/kywindow.cpp" line="435"/>
        <location filename="../src/kywindow.cpp" line="724"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="../src/kywindow.cpp" line="436"/>
        <location filename="../src/kywindow.cpp" line="720"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../src/kywindow.cpp" line="437"/>
        <location filename="../src/kywindow.cpp" line="716"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../src/kywindow.cpp" line="448"/>
        <source>Framework</source>
        <translation>框架</translation>
    </message>
    <message>
        <location filename="../src/kywindow.cpp" line="458"/>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <location filename="../src/kywindow.cpp" line="468"/>
        <source>Package</source>
        <translation>安装方式</translation>
    </message>
    <message>
        <location filename="../src/kywindow.cpp" line="478"/>
        <source>Platform</source>
        <translation>计算平台</translation>
    </message>
    <message>
        <location filename="../src/kywindow.cpp" line="501"/>
        <source>Install Info</source>
        <translation>安装信息</translation>
    </message>
    <message>
        <location filename="../src/kywindow.cpp" line="509"/>
        <source>Progress</source>
        <translation>安装进度</translation>
    </message>
    <message>
        <location filename="../src/kywindow.cpp" line="534"/>
        <location filename="../src/kywindow.cpp" line="805"/>
        <source>Install</source>
        <translation>安装</translation>
    </message>
    <message>
        <location filename="../src/kywindow.cpp" line="691"/>
        <source>Version: </source>
        <translation>版本： </translation>
    </message>
    <message>
        <location filename="../src/kywindow.cpp" line="865"/>
        <location filename="../src/kywindow.cpp" line="1087"/>
        <source>Install Canceled</source>
        <translation>安装已取消</translation>
    </message>
    <message>
        <location filename="../src/kywindow.cpp" line="922"/>
        <location filename="../src/kywindow.cpp" line="942"/>
        <location filename="../src/kywindow.cpp" line="961"/>
        <location filename="../src/kywindow.cpp" line="977"/>
        <source>Installing, selection disabled!</source>
        <translation>安装中，选择被禁用！</translation>
    </message>
    <message>
        <location filename="../src/kywindow.cpp" line="984"/>
        <source>No NVIDIA Card, CUDA disabled!</source>
        <translation>无NVIDIA显卡，不支持CUDA安装方式！</translation>
    </message>
    <message>
        <location filename="../src/kywindow.cpp" line="988"/>
        <source>NVIDIA Card version is too old, CUDA disabled!</source>
        <translation>NVIDIA显卡版本过低，不支持CUDA安装方式！</translation>
    </message>
    <message>
        <location filename="../src/kywindow.cpp" line="1100"/>
        <source>Install Successfully</source>
        <translation>安装成功</translation>
    </message>
    <message>
        <location filename="../src/kywindow.cpp" line="1116"/>
        <location filename="../src/kywindow.cpp" line="1165"/>
        <source>Install Failed</source>
        <translation>安装失败</translation>
    </message>
    <message>
        <location filename="../src/kywindow.cpp" line="114"/>
        <location filename="../src/kywindow.cpp" line="800"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../src/main.cpp" line="103"/>
        <source>AI Frame Installer</source>
        <translation>AI框架安装助手</translation>
    </message>
</context>
</TS>
