#ifndef KYWINDOW_H
#define KYWINDOW_H

#include <qdbusinterface.h>
#include <qtextbrowser.h>
#include <qnetworkaccessmanager.h>
#include <qsettings.h>
#include <qprocess.h>

#include <kwidget.h>
#include <kpushbutton.h>
#include <kprogressbar.h>
#include <ktabbar.h>
#include <kaboutdialog.h>

using namespace kdk;
//处理选项使能、切换相关的类
class AIStep : public QObject
{
public:
    ~AIStep();
    QStringList childrenNames;
    QVector<AIStep*> children;
    QString name;
    int defalutIndex = 0;
    int indent = 0;
    AIStep *parent = NULL;
};

//主界面类
class KyWindow : public KWidget
{
    Q_OBJECT

public:

    KyWindow(KWidget *parent = nullptr);
    ~KyWindow();

private slots:
    //安装按钮事件
    void on_installBtn_clicked(bool checked);

    //总线slot
    void slotStandOutput(QString output);
    void slotErrorOutput(QString output);
    void slotStart(bool status);
    void slotFinish(int exitCode, int exitStatus, bool isOut=true);
    void slotProgressInfo(QString output);
    void slotAppendProgressInfo(QString output);
    void slotProgressValue(int value);
    void slotOtherMsg(QString output);

    //tab相关按键处理
    void slotTabFrameClicked(int index);
    void slotTabVersionClicked(int index);
    void slotTabPackageClicked(int index);
    void slotTabPlateformClicked(int index);
    //update
    void slotVersionReplyFinished(QNetworkReply *reply);
//    void slotXMLReplayFinished(QNetworkReply *reply);

    //处理执行slot相关
    void slotExecStandOutput();
    void slotExecStandError();
    void slotExecStarted();
    void slotExecFinished(int exitCode, QProcess::ExitStatus exitStatus);
    void exec(QString cmd);
    void stopExec();

private:
    void closeEvent(QCloseEvent *event) override;

    //执行两个关键的脚本
    void executeAIAsst1();
    void executeAIAsst2();


    //检查以及消息处理函数
    void checkUpdate();
    bool checkAuth();
    void checkNvidiaCard();
    void checkKVM();
    bool shouldRestart();
    void checkExitCode(int exitCode);
    void messageRestart();
    void messageRetry();
    void messageKVMNotSupport();
    void messageWhenClose(QCloseEvent *event);

    void cancelInstallation();
    void installFrame();

    //更新选项相关函数
    void updateWidgetsStatus();
    void updateFrame(int index);
    void getDefaultFrame();
    void parseFrameXML();
    void initWidgets();
    void initDbus();
    void keepScreenAlive();
    void cancelKeepScreenAlive();

    //其他关于、帮助等函数
    void showAbout();
    void showHelp();
    void triggerMenu(QAction *act);


    //判断安装、显卡等相关bool变量
    bool _isInstalling = false;
    bool _isNvidiaCard = false;
    bool _isNvidiaCardOld = true;
    bool _isKVM = false;

    //保存版本等整数变量
    int _netFrameVersion;
    int _netAsst1Version;
    int _netAsst2Version;
    int _progressValue = 0;
    int _randCount = 0;
    int _stepInstall = 0;


    //进度条相关变量
    QString _progressInfo;
    QTimer *_timer;

    //持久化设置
    QSettings *_settings;
    //网络资源获取实例
    QNetworkAccessManager *_netManager;

    //与服务程序通信的总线接口
    QDBusInterface *_dbusInterface = nullptr;
    QDBusInterface *_dbusInterfaceSessionManager = nullptr;

    int _inhibit_value;

    //子程序执行进程，
    QProcess *_exec = nullptr;
    QString _standStr;
    QString _errorStr;

    //子程序反馈相关变量
    int _space = -1;
    int _execProgValue = 0;
    bool _isProgFree = true;

    //用以控制选项选择时高亮、使能、切换逻辑的类实例
    AIStep *_aiStepRoot;
    AIStep *_aiSteps[4];

    //用户控制及反馈相关UI
//    KDialog *dialogRestart = nullptr;
//    KAboutDialog *dialogAbout = nullptr;
    KPushButton *btnInstall = nullptr;
    KProgressBar *barProgress = nullptr;
    QLabel *labelProgress = nullptr;
    QTextBrowser *textInstall = nullptr;

    //实现选项选择的tab，并支持互相之间的高亮逻辑
    KTabBar *tabs[4];
    KTabBar *tabFrame = nullptr;
    KTabBar *tabVersion = nullptr;
    KTabBar *tabPackage = nullptr;
    KTabBar *tabPlateform = nullptr;
};
#endif // KYWINDOW_H
