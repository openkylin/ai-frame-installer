/*
 * InputLeap -- mouse and keyboard sharing utility
 * Copyright (C) 2012-2016 Symless Ltd.
 * Copyright (C) 2008 Volker Lanz (vl@fidra.de)
 *
 * This package is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * found in the file LICENSE that should have accompanied this file.
 *
 * This package is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if !defined(QBARRIERAPPLICATION__H)

#define MyApplication__H

#include <QApplication>
#include <QtNetwork/QLocalServer>

class QSessionManager;

//该类检查该程序是否以及运行，用来确保单例运行
class MyApplication : public QApplication
{
public:
   MyApplication(int& argc, char** argv);
    ~MyApplication();

    bool isRunning();

public:

    static MyApplication* getInstance();

private:


    static MyApplication* s_Instance;

    bool _isRunning;                // 是否已經有实例在运行
    QLocalServer *_localServer;     // 本地socket Server
    QString _serverName;            // 服务名称

    void _initLocalConnection();
    // 创建服务端
    void _newLocalServer();
    // 激活窗口
    void _activateWindow();
//private slots:
//    // 有新连接时触发
//    void _newLocalConnection();
};

#endif
