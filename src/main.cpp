#include "kywindow.h"
#include "myapplication.h"


#include <unistd.h>
#include <qtranslator.h>
#include <QLibraryInfo>
#include <qdir.h>


//判断是否有admin权限
bool isRunAsAdmin()
{
    return geteuid() == 0; // true, is root
}

//程序入口
int main(int argc, char *argv[])
{
//    if(!isRunAsAdmin())
//    {
//        system("pkexec bash -c \"python /home/lhh/桌面/ai.py\"");
//        return 0;
//    }

    setbuf(stdout, NULL);
    //适配高清分辨率
#if (QT_VERSION >= QT_VERSION_CHECK(5, 6, 0))
    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);
#endif

#if (QT_VERSION >= QT_VERSION_CHECK(5, 14, 0))
    QApplication::setHighDpiScaleFactorRoundingPolicy(Qt::HighDpiScaleFactorRoundingPolicy::PassThrough);
#endif

    MyApplication app(argc, argv);
    //判断是否已有该程序运行，已有则退出，确保单例运行
    if(app.isRunning())
    {
        return 0;
    }

    //加载翻译文件
    QTranslator trans;
    QString locale = QLocale::system().name();
    if(locale == "zh_CN")
    {
        if(trans.load(":/translations/gui_zh_CN.qm"))
        {
            app.installTranslator(&trans);
        }
    }
    if(locale == "bo_CN")
    {
        if(trans.load(":/translations/gui_bo_CN.qm"))
        {
            app.installTranslator(&trans);
        }
    }
    //翻译





    QTranslator qt_trans;
    QString qt_trans_path;
    qt_trans_path = QLibraryInfo::location(QLibraryInfo::TranslationsPath); // /usr/share/qt5/translations
    if (!qt_trans.load("qt_" + locale + ".qm", qt_trans_path)) {
        qDebug() << "Load translation file："
                 << "qt_" + locale + ".qm from" << qt_trans_path << "failed!";
    } else {
        app.installTranslator(&qt_trans);
    }

    QTranslator app_trans_penoy;
    app_trans_penoy.load("/usr/share/libpeony-qt/libpeony-qt_" + QLocale::system().name());
    app.installTranslator(&app_trans_penoy);


    QTranslator sdk_trans;
    if (sdk_trans.load(":/translations/gui_" + locale + ".qm")) {
        app.installTranslator(&sdk_trans);
    }

    QString trans_path;
    if (QDir("/usr/share/ai-frame-installer/translations").exists()) {
        trans_path = "/usr/share/ai-frame-installer/translations";
    } else {
        trans_path = qApp->applicationDirPath() + "/translations";
    }
    QTranslator app_trans;
    if (!app_trans.load("ai-frame-installer_" + locale + ".qm", trans_path)) {
        qDebug() << "Load translation file："
                 << "ai-frame-installer_" + locale + ".qm from" << trans_path << "failed!";
    } else {
        app.installTranslator(&app_trans);
    }


    //设置软件名及版本
    app.setApplicationName(QApplication::tr("AI Frame Installer"));
    app.setApplicationVersion("1.0.4-ok1");

    //创建主窗口并显示，继续运行程序
    KyWindow w;
    w.show();
    return app.exec();
}
