#ifndef PUB_DEF_H
#define PUB_DEF_H

//update
#define SERVER_ADDRESS "https://gitee.com/openkylin/ai-frame-installer/raw/master"
#define VERSION_FILE "UPDATE"
#define FRAME_XML_VERSION 24042400 //240104100 //23100100
#define AI_ASST1_VERSION 24042400
#define AI_ASST2_VERSION 24042400
#define PREFIX_XML_VERSION      "frame-xml-ver: "
#define PREFIX_ASST1_VERSION    "ai--asst1-ver: "
#define PREFIX_ASST2_VERSION    "ai--asst2-ver: "


//settings
#define SET_FRAME_XML_VERSION "frame-xml-version"
#define SET_AI_ASST1_VERSION "ai-asst1-version"
#define SET_AI_ASST2_VERSION "ai-asst2-version"
#define SET_IS_AUTH_INSTALL "isAuthInstall"
#define SET_RESTART_FLAG "restart-flag"
//signal
#define MSG_UPDATE_AI_FRAME "UpdateAIFrameSuccess"
#define MSG_UPDATE_AI_ASST1 "UpdateAIAsst1Success"
#define MSG_UPDATE_AI_ASST2 "UpdateAIAsst2Success"
#define MSG_PROG_FREE_START "ProgFreeStart"
#define MSG_PROG_FREE_END "ProgFreeEnd"

//feedback
#define RESTART_CODE 66

#endif // KYWINDOW_H
