#include "kywindow.h"

#include "kwidget.h"
#include "pubdef.h"

#include <qgridlayout.h>
#include <qapplication.h>
#include <user_manual.hpp>
#include <qxmlstream.h>
#include <qsettings.h>
#include <QNetworkReply>
#include <qprocess.h>
#include <qdir.h>
#include <qtimer.h>

#include <kmessagebox.h>
#include <qdbusreply.h>
#include <qscreen.h>
#include "nvidia535.h"
using namespace kdk;

//todo
//检测docker是否支持，https://docs.docker.com/desktop/install/linux-install/

//释放资源
AIStep::~AIStep()
{
    for(int i=children.size()-1;i>-1;i--)
    {
        AIStep *child = children[i];
        delete child;
    }
    //    printf("delete AIStep: %s\n", qPrintable(name));
}


//检查更新
void KyWindow::checkUpdate()
{
    _netManager = new QNetworkAccessManager(this);    //新建QNetworkAccessManager对象
    connect(_netManager,SIGNAL(finished(QNetworkReply*)), this, SLOT(slotVersionReplyFinished(QNetworkReply*))); //关联信号和槽
    QString urlPath = QString("%1/src/%2").arg(SERVER_ADDRESS).arg(VERSION_FILE);
    QNetworkReply *networkReply = _netManager->get(QNetworkRequest(QUrl(urlPath)));//发送请求

}

//需要重启时提示
void KyWindow::messageRestart()
{
    if(!shouldRestart())
    {
        return;
    }
    KMessageBox msgBox(this);
    msgBox.setFixedSize(424,164);
    msgBox.setCustomIcon(QIcon::fromTheme("ukui-dialog-success"));
    msgBox.setWindowTitle("");
    msgBox.setText(tr("Install successfully, restart to apply the opreation?"));

    KPushButton btnOK(&msgBox);
    btnOK.setBackgroundColorHighlight(true);
    btnOK.setText(tr("Restart"));
    msgBox.addButton(&btnOK, KMessageBox::YesRole);
    msgBox.setDefaultButton(&btnOK);
    KPushButton btnCancel(&msgBox);
    btnCancel.setText(tr("Restart Later"));
    msgBox.addButton(&btnCancel, KMessageBox::NoRole);
    msgBox.setParent(this);

    msgBox.move(this->geometry().center().x() - msgBox.width() / 2,
                this->geometry().center().y() - msgBox.height() / 2);
    if(msgBox.exec() == 0)
    {
        QAbstractButton* btn = msgBox.clickedButton();
        if(btn == &btnOK)
        {
//            _settings->setValue(SET_RESTART_FLAG, false);
            system("ukui-session-tools --reboot");
        }
    }
}

//重试时提示
void KyWindow::messageRetry()
{
    KMessageBox msgBox(this);
    msgBox.setFixedSize(424,164);
    msgBox.setIcon(KMessageBox::Icon::Warning);
    msgBox.setWindowTitle("");
    msgBox.setText(tr("Install failed, please reselect and retry."));

    KPushButton btnOK(&msgBox);
    btnOK.setText(tr("OK"));
    msgBox.addButton(&btnOK, KMessageBox::YesRole);
    msgBox.setDefaultButton(&btnOK);

    msgBox.move(this->geometry().center().x() - msgBox.width() / 2,
                this->geometry().center().y() - msgBox.height() / 2);
    msgBox.exec();
}


//正在安装时，关闭程序提示
void KyWindow::messageWhenClose(QCloseEvent *event)
{
    KMessageBox msgBox(this);
    msgBox.setFixedSize(424,164);
    msgBox.setIcon(KMessageBox::Icon::Warning);
    msgBox.setWindowTitle("");
    msgBox.setText(tr("Framework is installing, are you sure you want to quit?"));

    KPushButton btnCancel(&msgBox);
    btnCancel.setBackgroundColorHighlight(true);
    btnCancel.setText(tr("Cancel"));
    msgBox.addButton(&btnCancel, KMessageBox::YesRole);
    msgBox.setDefaultButton(&btnCancel);
    KPushButton btnQuit(&msgBox);
    btnQuit.setText(tr("Quit Now"));
    msgBox.addButton(&btnQuit, KMessageBox::NoRole);
    msgBox.setParent(this);

    msgBox.move(this->geometry().center().x() - msgBox.width() / 2,
                this->geometry().center().y() - msgBox.height() / 2);
    if(msgBox.exec() == 0)
    {
        QAbstractButton* btn = msgBox.clickedButton();
        if(btn == &btnQuit)
        {
            cancelInstallation();
            return;
        }
    }
    event->ignore();
}

//KVM不支持时提示
void KyWindow::messageKVMNotSupport()
{
    KMessageBox msgBox(this);
    msgBox.setFixedSize(424,164);
    msgBox.setIcon(KMessageBox::Icon::Warning);
    msgBox.setWindowTitle("");
    msgBox.setText(tr("KVM and package docker is not supported, please select other package."));

    KPushButton btnOK(&msgBox);
    btnOK.setText(tr("OK"));
    msgBox.addButton(&btnOK, KMessageBox::YesRole);
    msgBox.setDefaultButton(&btnOK);

    msgBox.move(this->geometry().center().x() - msgBox.width() / 2,
                this->geometry().center().y() - msgBox.height() / 2);
    msgBox.exec();
}
//由于系统环境设置不再需要重启，重启只取决与显卡驱动是否生效。恢复该方式，nvidia-driver机器安装完后检测nvidia-smi是否有效确定是否重启
//判断是否需要重启
bool KyWindow::shouldRestart()
{
    if(!_isNvidiaCard)
    {
        return false;
    }
    QProcess *process = new QProcess(this);
    process->start("bash -c \"nvidia-smi | grep 'NVIDIA-SMI has failed'\"");
    process->waitForFinished();
    QString result = QString(process->readAll());
    process->deleteLater();
    if(result.contains("NVIDIA-SMI has failed"))
    {
       return true;
    }
    return false;
}


//根据脚本执行结束时的返回代码给出相关提示
void KyWindow::checkExitCode(int exitCode)
{
    if(exitCode == RESTART_CODE || exitCode == 0)
    {
        messageRestart();
    }
    else if(exitCode != 0)
    {
        messageRetry();
    }
}

//检查英伟达显卡
void KyWindow::checkNvidiaCard()
{
    QProcess *process = new QProcess(this);
    process->start("bash -c \"lspci | grep -i vga | grep -i nvidia\"");
    process->waitForFinished();
    QString result = QString(process->readAll());
    if(result.contains("nvidia", Qt::CaseSensitivity::CaseInsensitive))
    {
        _isNvidiaCard = true;
        if(nvidia535andcuda117support(result))
        {
            _isNvidiaCardOld = false;
        }
    }
    process->deleteLater();
}

//检查cpu是否支持虚拟化
void KyWindow::checkKVM()
{
    //另一种方法
    //apt install cpu-checker

    QProcess *process = new QProcess(this);
    process->start("bash -c \"lsmod | grep kvm\"");
    process->waitForFinished();
    QString result = QString(process->readAll());
    if(result.contains("kvm_intel") || result.contains("kvm_amd"))
    {
        _isKVM = true;
    }
    process->deleteLater();
}

//网络请求信号结束返回 反馈版本相关信息
void KyWindow::slotVersionReplyFinished(QNetworkReply *reply)
{
#ifdef QT_DEBUG
    printf("slotVersionReplyFinished\n");
#endif

    int frameXMLVersion = _settings->value(SET_FRAME_XML_VERSION, FRAME_XML_VERSION).toInt();
    frameXMLVersion = frameXMLVersion < FRAME_XML_VERSION ? FRAME_XML_VERSION : frameXMLVersion;
    int asst1Version = _settings->value(SET_AI_ASST1_VERSION, AI_ASST1_VERSION).toInt();
    asst1Version = asst1Version < AI_ASST1_VERSION ? AI_ASST1_VERSION : asst1Version;
    int asst2Version = _settings->value(SET_AI_ASST2_VERSION, AI_ASST2_VERSION).toInt();
    asst2Version = asst2Version < AI_ASST2_VERSION ? AI_ASST2_VERSION : asst2Version;

    QString str = QString(reply->readAll());
    QString md5Str;
    QStringList strList = str.split('\n');
    foreach(QString tStr, strList)
    {
        if(tStr.size() != 56)
        {
            continue;
        }
#ifdef QT_DEBUG
        printf("%s\n", qPrintable(tStr));
#endif
        if(tStr.startsWith(PREFIX_XML_VERSION))
        {
            _netFrameVersion = tStr.mid(15, 8).toInt();
            md5Str = tStr.mid(24, -1);

            if(_netFrameVersion>frameXMLVersion)
            {
                if (_dbusInterface->isValid()) {
                    QString cmd = QString("bash -c \"cd /usr/share/ai-frame-installer && wget %1/src/aiframe.xml -O aiframe_bak.xml &> /dev/null && echo $(md5sum aiframe_bak.xml) | grep %2 && mv -f aiframe_bak.xml aiframe.xml\"").arg(SERVER_ADDRESS).arg(md5Str);
                    _dbusInterface->call("tempExec",cmd);
                }
            }
        }
        else if(tStr.startsWith(PREFIX_ASST1_VERSION))
        {
            _netAsst1Version = tStr.mid(15, 8).toInt();
            md5Str = tStr.mid(24, -1);
            if( _netAsst1Version>asst1Version)
            {
                if (_dbusInterface->isValid()) {
                    QString cmd = QString("bash -c \"cd /usr/share/ai-frame-installer && wget %1/kylin-aiassistant/ai-asst1 -O ai-asst1_bak &> /dev/null && echo $(md5sum ai-asst1_bak) | grep %2 && mv -f ai-asst1_bak ai-asst1\"").arg(SERVER_ADDRESS).arg(md5Str);
                    _dbusInterface->call("tempExec",cmd);
                }
            }
        }
        else if(tStr.startsWith(PREFIX_ASST2_VERSION))
        {
            _netAsst2Version = tStr.mid(15, 8).toInt();
            md5Str = tStr.mid(24, -1);
            if( _netAsst2Version>asst2Version)
            {
                if (_dbusInterface->isValid()) {
                    QString cmd = QString("bash -c \"cd /usr/share/ai-frame-installer && wget %1/kylin-aiassistant/ai-asst2 -O ai-asst2_bak &> /dev/null && echo $(md5sum ai-asst2_bak) | grep %2 && mv -f ai-asst2_bak ai-asst2\"").arg(SERVER_ADDRESS).arg(md5Str);
                    _dbusInterface->call("tempExec",cmd);
                }
            }
        }
    }
}

//更新第index层的选项状态
void KyWindow::updateFrame(int index)
{
    //异常退出
    for(;index<4;index++)
    {
        int diff = _aiSteps[index]->children.size() - tabs[index]->count();
        int i = diff>0?diff:-diff;

        for (; i>0; i--)
        {
            if(diff>0)
            {
                tabs[index]->addTab(" ");
            }
            else
            {
                tabs[index]->removeTab(0);
            }
        }
        foreach(QString name, _aiSteps[index]->childrenNames)
        {
            tabs[index]->setTabText(i++, name);
        }
        if(index==3 && (!_isNvidiaCard || _isNvidiaCardOld))
        {
            tabPlateform->setTabEnabled(tabPlateform->count()-1, true);
            for(int k=0;k<tabPlateform->count()-1;k++)
            {
                tabPlateform->setTabEnabled(k, false);
            }
            tabPlateform->setCurrentIndex(tabPlateform->count()-1);
        }
        else
        {
            tabs[index]->setCurrentIndex(_aiSteps[index]->defalutIndex);
        }

    }
}
void KyWindow::getDefaultFrame()
{
    //todo
}

//解析默认选项状态并更新
void KyWindow::parseFrameXML()
{
    QFile file("/usr/share/ai-frame-installer/aiframe.xml");
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qWarning("open file aiframe.xml failed");
        getDefaultFrame();
        return;
    }
    //定义解析器对象
    QXmlStreamReader xmlReader(&file);

    _aiStepRoot = new AIStep;
    AIStep *curStep = _aiStepRoot;
    int indent = 0;
    //解析xml，直到结束或发送错误
    while (!xmlReader.atEnd() && !xmlReader.hasError())
    {
        QXmlStreamReader::TokenType type = xmlReader.readNext();
        if (type == QXmlStreamReader::TokenType::StartElement)
        {
#ifdef QT_DEBUG
            printf("%s%s", qPrintable(QString(indent, ' ')), qPrintable(xmlReader.name().toString()));
#endif
            if(xmlReader.name() != "Root")
            {
                AIStep *nStep = new AIStep();
                if(indent > curStep->indent)
                {
                    curStep->children.append(nStep);
                    nStep->parent = curStep;
                }
                else
                {
                    AIStep *parent = curStep->parent;
                    int count = curStep->indent - indent;
                    for(int i=0;i<count;i++)
                    {
                        parent = parent->parent;
                    }
                    parent->children.append(nStep);
                    nStep->parent = parent;
                }
                curStep = nStep;
            }
            curStep->name = xmlReader.name().toString();
            curStep->indent = indent;

            QXmlStreamAttributes attrs = xmlReader.attributes();
            foreach(QXmlStreamAttribute attr, attrs)
            {
                //结果
                QString sNodeName = attr.name().toString();  //获得属性名;
                QString sNodeValue = attr.value().toString(); //获得属性值;
#ifdef QT_DEBUG
                printf(" %s:%s", qPrintable(sNodeName),qPrintable(sNodeValue));
#endif
                if(sNodeName == "name")
                {
                    curStep->parent->childrenNames.append(sNodeValue);
                }
                else if(sNodeName == "default")
                {
                    curStep->parent->defalutIndex = curStep->parent->children.size()-1;
                }

            }
#ifdef QT_DEBUG
            printf("\n");
#endif
            indent++;
        }
        else if (type == QXmlStreamReader::TokenType::EndElement)
        {
            indent--;
        }
    }
    file.close();

    _aiSteps[0] = _aiStepRoot;
    for(int i=1;i<4;i++)
    {
        _aiSteps[i] = _aiSteps[i-1]->children[_aiSteps[i-1]->defalutIndex];
    }
}

//初始化各个组件
void KyWindow::initWidgets()
{
    setWidgetName(QApplication::applicationName());
    setIcon(QIcon::fromTheme("ai-frame-installer", QIcon("/usr/share/pixmaps/ai-frame-installer.png")));
    setFixedSize(600,488);
    //    setFixedSize(578,488);
    //    setFixedSize(546,468);
    KWindowButtonBar *barWinBtn = windowButtonBar();
    barWinBtn->maximumButton()->hide();
    barWinBtn->menuButton()->settingAction()->setVisible(false);
    barWinBtn->menuButton()->themeAction()->setVisible(false);

    barWinBtn->menuButton()->assistAction()->setVisible(false);
    barWinBtn->menuButton()->assistAction()->setText(tr("Help"));
    barWinBtn->menuButton()->aboutAction()->setText(tr("About"));
    barWinBtn->menuButton()->quitAction()->setText(tr("Quit"));

    connect(barWinBtn->menuButton()->menu(), &QMenu::triggered, this, &KyWindow::triggerMenu);

    parseFrameXML();
    QWidget *widget = baseBar();
    QGridLayout *gridLayout = new QGridLayout(widget);
    gridLayout->setContentsMargins(32, 22, 32, 22);
    gridLayout->setHorizontalSpacing(32);
    gridLayout->setVerticalSpacing(16);

    gridLayout->addWidget( new QLabel(tr("Framework"),widget), 0, 0);
    tabFrame = new KTabBar(SegmentDark, widget);
    tabFrame->setFixedHeight(36);
    foreach(QString name, _aiSteps[0]->childrenNames)
    {
        tabFrame->addTab(name);
    }
    tabFrame->setCurrentIndex(_aiSteps[0]->defalutIndex);
    gridLayout->addWidget(tabFrame, 0, 1);

    gridLayout->addWidget( new QLabel(tr("Version"),widget), 1, 0);
    tabVersion = new KTabBar(SegmentDark, widget);
    tabVersion->setFixedHeight(36);
    foreach(QString name, _aiSteps[1]->childrenNames)
    {
        tabVersion->addTab(name);
    }
    tabVersion->setCurrentIndex(_aiSteps[1]->defalutIndex);
    gridLayout->addWidget(tabVersion, 1, 1);

    gridLayout->addWidget( new QLabel(tr("Package"),widget), 2, 0);
    tabPackage = new KTabBar(SegmentDark, widget);
    tabPackage->setFixedHeight(36);
    foreach(QString name, _aiSteps[2]->childrenNames)
    {
        tabPackage->addTab(name);
    }
    tabPackage->setCurrentIndex(_aiSteps[2]->defalutIndex);
    gridLayout->addWidget(tabPackage, 2, 1);

    gridLayout->addWidget( new QLabel(tr("Platform"),widget), 3, 0);
    tabPlateform = new KTabBar(SegmentDark, widget);
    foreach(QString name, _aiSteps[3]->childrenNames)
    {
        tabPlateform->addTab(name);
    }

    if(!_isNvidiaCard || _isNvidiaCardOld)
    {
        tabPlateform->setTabEnabled(tabPlateform->count()-1, true);
        for(int k=0;k<tabPlateform->count()-1;k++)
        {
            tabPlateform->setTabEnabled(k, false);
        }
        tabPlateform->setCurrentIndex(tabPlateform->count()-1);
    }
    else
    {
        tabPlateform->setCurrentIndex(_aiSteps[3]->defalutIndex);
    }

    gridLayout->addWidget(tabPlateform, 3, 1);

    gridLayout->addWidget( new QLabel(tr("Install Info"),widget), 4, 0, 3, 1);
    textInstall = new QTextBrowser(widget);
    textInstall->setMinimumHeight(96);
//    textInstall->setFont();
    //    textInstall->setMinimumHeight(64);
    //    textInstall->setFixedHeight(64);
    gridLayout->addWidget(textInstall, 4, 1,3, 1);

    gridLayout->addWidget( new QLabel(tr("Progress"),widget), 7, 0);
    QVBoxLayout *vBoxLayout = new QVBoxLayout(nullptr);
    //    vBoxLayout->SetFixedSize();
    vBoxLayout->setContentsMargins(0, 0, 0, 0);
    vBoxLayout->setSpacing(4);
    barProgress = new KProgressBar(widget);
    barProgress->setFixedHeight(14);
    barProgress->setValue(0);
    barProgress->setHidden(true);
    QFont font1 = barProgress->font();
    font1.setPointSize(8);
    barProgress->setFont(font1);

    vBoxLayout->addWidget(barProgress, 0);
    labelProgress = new QLabel("",widget);
    labelProgress->setFixedHeight(14);
    QFont font2 = labelProgress->font();
    font2.setPointSize(8);
    labelProgress->setFont(font2);
    vBoxLayout->addWidget(labelProgress, 1);
    gridLayout->addLayout(vBoxLayout, 7, 1);


    btnInstall = new KPushButton(widget);
    btnInstall->setBackgroundColorHighlight(true);
    btnInstall->setText(tr("Install"));
    connect(btnInstall, SIGNAL(clicked(bool)), this, SLOT(on_installBtn_clicked(bool)));
    gridLayout->addWidget(btnInstall, 8, 0, 1, 2, Qt::AlignCenter);


    connect(tabFrame, SIGNAL(tabBarClicked(int)) , this , SLOT(slotTabFrameClicked(int)));
    connect(tabVersion, SIGNAL(tabBarClicked(int)) , this , SLOT(slotTabVersionClicked(int)));
    connect(tabPackage, SIGNAL(tabBarClicked(int)) , this , SLOT(slotTabPackageClicked(int)));
    connect(tabPlateform, SIGNAL(tabBarClicked(int)) , this , SLOT(slotTabPlateformClicked(int)));
    tabs[0] = tabFrame;
    tabs[1] = tabVersion;
    tabs[2] = tabPackage;
    tabs[3] = tabPlateform;


    //应用居中
    if (QGuiApplication::platformName().startsWith(QLatin1String("wayland"), Qt::CaseInsensitive)) {
        int sw = QGuiApplication::primaryScreen()->availableGeometry().width();
        int sh = QGuiApplication::primaryScreen()->availableGeometry().height();
        this->move((sw - this->width()) / 2, (sh - this->height()) / 2);
    } else {
        QScreen *screen = QGuiApplication::primaryScreen();
        this->move((screen->geometry().width() - this->width()) / 2,
                   (screen->geometry().height() - this->height()) / 2);
    }
}

//初始化总线及信号槽
void KyWindow::initDbus()
{
    _dbusInterface = new QDBusInterface("com.kylin.aiassistant", "/",
                                        "com.kylin.aiassistant", QDBusConnection::systemBus());

    connect(_dbusInterface, SIGNAL(sigStandardOutput(QString)) , this , SLOT(slotStandOutput(QString)));
    connect(_dbusInterface, SIGNAL(sigStandardError(QString)) , this , SLOT(slotErrorOutput(QString)));
    connect(_dbusInterface, SIGNAL(sigExecStarted(bool)) , this , SLOT(slotStart(bool)));
    connect(_dbusInterface, SIGNAL(sigExecFinished(int, int)) , this , SLOT(slotFinish(int ,int)));
    connect(_dbusInterface, SIGNAL(sigProgressInfo(QString)) , this , SLOT(slotProgressInfo(QString)));
    connect(_dbusInterface, SIGNAL(sigAppendProgressInfo(QString)) , this , SLOT(slotAppendProgressInfo(QString)));
    connect(_dbusInterface, SIGNAL(sigProgressValue(int)) , this , SLOT(slotProgressValue(int)));
    connect(_dbusInterface, SIGNAL(sigOtherMsg(QString)) , this , SLOT(slotOtherMsg(QString)));
    if (_dbusInterface->isValid()) {
        _dbusInterface->call("stopExec");
    }
}

#define MAX_SERVICES 1
//取消主机不进入睡眠状态
void KyWindow::cancelKeepScreenAlive()
{
    if (_dbusInterfaceSessionManager->isValid())
    {
        _dbusInterfaceSessionManager->call("Uninhibit", _inhibit_value);
    }
}

//确保主机不进入睡眠状态
void KyWindow::keepScreenAlive()
{
    if(_dbusInterfaceSessionManager == nullptr)
    {
        _dbusInterfaceSessionManager = new QDBusInterface("org.gnome.SessionManager",
                                                          "/org/gnome/SessionManager",
                                                          "org.gnome.SessionManager",
                                                          QDBusConnection::sessionBus());
    }
    if (_dbusInterfaceSessionManager->isValid()) {

        QDBusMessage reply = _dbusInterfaceSessionManager->call(QDBus::Block, "Inhibit", "ai-frame-installer", (quint32)0, "frame is installing", (quint32)8);
        _inhibit_value = reply.arguments().takeFirst().toUInt();
#ifdef QT_DEBUG
        printf("keepScreenAlive replay: %d\n", _inhibit_value);
#endif

    }
}

//主窗口构建及初始化
KyWindow::KyWindow(KWidget *parent)
    : KWidget(parent)
{
    _settings = new QSettings("openKylin", "ai-frame-installer");
    initDbus();
    checkUpdate();
    checkNvidiaCard();
    initWidgets();

    _timer = new QTimer(this);
    srand(time(NULL));
    connect(_timer, &QTimer::timeout,[=]()
    {
        if(_randCount==0)
        {
            barProgress->setValue(_progressValue);
            _progressValue+=rand()%3;//进度变化大小 动态
            if(_progressValue>99)
            {
                _progressValue = 0;
            }
            _randCount = rand()%9+2;//0.4s~2s触发进度变化 动态
        }
        _randCount--;
    });

}

//响应关闭事件
void KyWindow::closeEvent(QCloseEvent *event)
{
    if(_isInstalling)
    {
        messageWhenClose(event);
    }
}

//主窗口析构释放相关资源
KyWindow::~KyWindow()
{
    printf("quit now\n");
    if (_dbusInterface != nullptr) {
        if (_dbusInterface->isValid()) {
            _dbusInterface->call("killAIAssistant");
        }
        _dbusInterface->deleteLater();
        _dbusInterface = nullptr;
    }
    if(_dbusInterfaceSessionManager != nullptr)
    {
        if (_dbusInterfaceSessionManager->isValid())
        {
            _dbusInterfaceSessionManager->call("Uninhibit", _inhibit_value);
        }
        _dbusInterfaceSessionManager->deleteLater();
        _dbusInterfaceSessionManager = nullptr;
    }
    stopExec();
    _timer->stop();
    delete _timer;
    delete _netManager;
    delete _aiStepRoot;
    delete _settings;
    printf("quit successfully\n");
}

//显示本软件About信息
void KyWindow::showAbout()
{
    //    if(dialogAbout == nullptr)
    //    {
    KAboutDialog dialogAbout(this);
    dialogAbout.setFixedSize(380,312);
    dialogAbout.setBodyTextVisiable(true);


    dialogAbout.setAppIcon(QIcon::fromTheme("ai-frame-installer", QIcon("/usr/share/pixmaps/ai-frame-installer.png")));

    dialogAbout.setAppName(QApplication::applicationName());
    dialogAbout.setAppVersion(tr("Version: ")+QApplication::applicationVersion());
    dialogAbout.setAppSupport("support@kylinos.cn");
    //    }
#ifdef QT_DEBUG
    printf("%d, %d\n",dialogAbout.width(), dialogAbout.height());
#endif
    dialogAbout.move(this->geometry().center().x() - dialogAbout.width() /2,
                     this->geometry().center().y() - dialogAbout.height() / 2);
    dialogAbout.exec();
}


//显示本软件帮助信息
void KyWindow::showHelp()
{
    kdk::kabase::UserManualManagement userManual;
    if (!userManual.callUserManual("ai-frame-installer")) {
        qCritical() << "user manual call fail!";
    }
}

//响应主界面右上方的菜单事件
void KyWindow::triggerMenu(QAction *act)
{
    QString str = act->text();
    if (tr("Quit") == str)
    {
        close();
    }
    else if (tr("About") == str)
    {
        showAbout();
    }
    else if (tr("Help") == str)
    {
        showHelp();
    }
}

//检查用户是否拥有权限
bool KyWindow::checkAuth()
{
    bool isAuth = _settings->value(SET_IS_AUTH_INSTALL, false).toBool();
    if(!isAuth)
    {
        QProcess *process = new QProcess(this);
        process->start("pkexec bash -c \"mkdir -p /usr/share/ai-frame-installer && echo Auth-Successfully > /usr/share/ai-frame-installer/auth \"");
        process->waitForFinished();
        int exitCode = process->exitCode();
        if(exitCode == 0)
        {
            isAuth = true;
            _settings->setValue(SET_IS_AUTH_INSTALL, true);
        }
        process->deleteLater();
    }
    return isAuth;
}


//更新各个组件状态
void KyWindow::updateWidgetsStatus()
{
    for(int k=0;k<tabFrame->count();k++)
    {
        if(tabFrame->currentIndex() != k)
        {
            tabFrame->setTabEnabled(k, !_isInstalling);
        }
    }
    for(int k=0;k<tabVersion->count();k++)
    {
        if(tabVersion->currentIndex() != k)
        {
            tabVersion->setTabEnabled(k, !_isInstalling);
        }
    }
    for(int k=0;k<tabPackage->count();k++)
    {
        if(tabPackage->currentIndex() != k)
        {
            tabPackage->setTabEnabled(k, !_isInstalling);
        }
    }
    for(int k=0;k<tabPlateform->count();k++)
    {
        if(tabPlateform->currentIndex() != k)
        {
            tabPlateform->setTabEnabled(k, !_isInstalling);
        }
    }

    if(!_isInstalling && (!_isNvidiaCard || _isNvidiaCardOld))
    {
        tabPlateform->setTabEnabled(tabPlateform->count()-1, true);
        for(int k=0;k<tabPlateform->count()-1;k++)
        {
            tabPlateform->setTabEnabled(k, false);
        }
    }
    //    tabFrame->setEnabled(!_isInstalling);
    //    tabVersion->setEnabled(!_isInstalling);
    //    tabPackage->setEnabled(!_isInstalling);
    //    tabPlateform->setEnabled(!_isInstalling);
    if(_isInstalling)
    {
        barProgress->setHidden(false);
        barProgress->setState(NormalProgress);
        labelProgress->setText("");
        btnInstall->setText(tr("Cancel"));
    }
    else
    {
        _timer->stop();
        btnInstall->setText(tr("Install"));
    }
}

//执行脚本第一阶段
void KyWindow::executeAIAsst1()
{
    //    :h:u:f:v:p:c:d

    QString cmd = QString("bash /usr/share/ai-frame-installer/ai-asst1");
    cmd.append(" -h " + QDir::homePath() + " -u " + QDir::home().dirName());
    cmd.append(" -f " + tabs[0]->tabText(tabs[0]->currentIndex()));
    if(tabs[1]->currentIndex()==1)
    {
        cmd.append(" -v pre");
    }
    cmd.append(" -p " + tabs[2]->tabText(tabs[2]->currentIndex()));

    //        cmd.append(" -c ANY");
    if(!_isNvidiaCard || _isNvidiaCardOld)
    {
        cmd.append(" -c CPU");
    }
    else
    {
        QString platform = tabs[3]->tabText(tabs[3]->currentIndex());
        if(platform.contains("CUDA"))
        {
            if(platform.contains("CPU"))
            {
                cmd.append(" -c ANY");
            }
            else
            {
                cmd.append(" -c " + platform.mid(4,-1));
            }
        }
        else
        {
            cmd.append(" -c CPU");
        }
    }
    if (_dbusInterface->isValid()) {

#ifdef QT_DEBUG
        printf("%s\n",qPrintable(cmd));
#endif
        _isInstalling = true;
        _stepInstall = 1;
        updateWidgetsStatus();
        _dbusInterface->call("exec", cmd);
    }
}

//取消安装
void KyWindow::cancelInstallation()
{
    if(_isInstalling)
    {
        textInstall->append("\nInstall Canceled");
        labelProgress->setText(tr("Install Canceled"));
        barProgress->setState(NormalProgress);
        _isInstalling = false;
        updateWidgetsStatus();
        if (_dbusInterface->isValid())
        {
            _dbusInterface->call("stopExec");
        }
        stopExec();
        cancelKeepScreenAlive();
    }
}

//开始安装框架
void KyWindow::installFrame()
{
    if(!checkAuth())
    {
        return;
    }
    if(!_isKVM)
    {
        if(tabs[2]->tabText(tabs[2]->currentIndex()) == "docker")
        {
            messageKVMNotSupport();
            return;
        }
    }

    textInstall->setText("");
    textInstall->update();
    barProgress->setValue(0);

    executeAIAsst1();

    keepScreenAlive();
}

//点击安装按钮事件
void KyWindow::on_installBtn_clicked(bool checked)
{
    if(_isInstalling)
    {
        cancelInstallation();
    }
    else
    {
        installFrame();
    }
}


//点击框架选项事件
void KyWindow::slotTabFrameClicked(int index)
{
    if(_isInstalling && tabs[0]->currentIndex() != index)
    {
        textInstall->append(tr("Installing, selection disabled!"));
        return;
    }
    if(tabs[0]->currentIndex() == index || index == -1)
    {
        return;
    }
    _aiSteps[1] = _aiSteps[0]->children[index];
    for(int i=2;i<4;i++)
    {
        _aiSteps[i] = _aiSteps[i-1]->children[_aiSteps[i-1]->defalutIndex];
    }
    updateFrame(1);
}

//点击版本选项事件
void KyWindow::slotTabVersionClicked(int index)
{
    if(_isInstalling && tabs[1]->currentIndex() != index)
    {
        textInstall->append(tr("Installing, selection disabled!"));
        tabVersion->setCurrentIndex(index);
        return;
    }
    if(tabs[1]->currentIndex() == index || index == -1)
    {
        return;
    }

    _aiSteps[2] = _aiSteps[1]->children[index];
    _aiSteps[3] = _aiSteps[2]->children[_aiSteps[2]->defalutIndex];
    updateFrame(2);
}

//点击安装方式选项事件
void KyWindow::slotTabPackageClicked(int index)
{
    if(_isInstalling && tabs[2]->currentIndex() != index)
    {
        textInstall->append(tr("Installing, selection disabled!"));
        return;
    }
    if(tabs[2]->currentIndex() == index || index == -1)
    {
        return;
    }
    _aiSteps[3] = _aiSteps[2]->children[index];
    updateFrame(3);
}

//点击安装平台选项事件
void KyWindow::slotTabPlateformClicked(int index)
{
    if(_isInstalling && tabs[3]->currentIndex() != index)
    {
        textInstall->append(tr("Installing, selection disabled!"));
        return;
    }
    if((!_isNvidiaCard || _isNvidiaCardOld) && index != tabPlateform->count()-1)
    {
        if(!_isNvidiaCard)
        {
            textInstall->append(tr("No NVIDIA Card, CUDA disabled!"));
        }
        else if(_isNvidiaCardOld)
        {
            textInstall->append(tr("NVIDIA Card version is too old, CUDA disabled!"));
        }

    }
    if(tabs[1]->currentIndex() == index || index == -1)
    {
        return;
    }

}

//输出执行脚本反馈的标准输出信息
void KyWindow::slotStandOutput(QString output)
{
    if(!_isInstalling)
    {
        return;
    }
    //    qInfo() << output;
    //    textInstall->setTextColor(QColor::fromRgb(0, 0, 0));//无法跟随主题，暂时放弃


    while(output.contains("\033[A"))
    {
        output.replace("\033[A","");
    }
    textInstall->append(output);
    textInstall->moveCursor(QTextCursor::End);
}

//输出执行脚本反馈的错误信息
void KyWindow::slotErrorOutput(QString output)
{
    if(!_isInstalling)
    {
        return;
    }
    //    qInfo() << output;
//        textInstall->setTextColor(QColor::fromRgb(1, 0, 0));//无法跟随主题，暂时放弃
    textInstall->append(output);
}
void KyWindow::slotStart(bool status)
{
    //    textInstall->append("Begin Install");
}

//执行脚本第2阶段
void KyWindow::executeAIAsst2()
{
    QString cmd = QString("bash /usr/share/ai-frame-installer/ai-asst2");
    cmd.append(" -h " + QDir::homePath() + " -u " + QDir::home().dirName());
    cmd.append(" -f " + tabs[0]->tabText(tabs[0]->currentIndex()));
    if(tabs[1]->currentIndex()==1)
    {
        cmd.append(" -v pre");
    }
    cmd.append(" -p " + tabs[2]->tabText(tabs[2]->currentIndex()));

    if(!_isNvidiaCard || _isNvidiaCardOld)
    {
        cmd.append(" -c CPU");
    }
    else
    {
        QString platform = tabs[3]->tabText(tabs[3]->currentIndex());
        if(platform.contains("CUDA"))
        {
            if(platform.contains("CPU"))
            {
                cmd.append(" -c ANY");
            }
            else
            {
                cmd.append(" -c " + platform.mid(4,-1));
            }
        }
        else
        {
            cmd.append(" -c CPU");
        }
    }
    exec(cmd);
    _stepInstall = 2;
}

//脚本执行结束时的相关处理
void KyWindow::slotFinish(int exitCode, int exitStatus, bool isOut)
{
#ifdef QT_DEBUG
    printf("exitCode: %d\n",exitCode);
#endif
    if(!_isInstalling)
    {
        return;
    }
    if(exitStatus==1)
    {
        if(!isOut)
        {
            labelProgress->setText(tr("Install Canceled"));

            barProgress->setState(NormalProgress);
        }
    }
    else if(exitCode==0 || exitCode==RESTART_CODE)
    {
//        if(exitCode==RESTART_CODE)
//        {
//            _settings->setValue(SET_RESTART_FLAG, true);
//        }
        if(!isOut)
        {
            labelProgress->setText(tr("Install Successfully"));
            barProgress->setValue(100);
            //             barProgress->setState(SuccessProgress);
            _isInstalling = false;
            updateWidgetsStatus();
            checkExitCode(exitCode);

        }
        else
        {
            executeAIAsst2();
        }
    }
    else
    {
        textInstall->append(QString("exitCode: %1").arg(exitCode));
        labelProgress->setText(tr("Install Failed"));
        barProgress->setState(FailedProgress);
        _isInstalling = false;
        updateWidgetsStatus();
    }

}


//显示脚本执行时的进度状态信息
void KyWindow::slotProgressInfo(QString output)
{
    if(!_isInstalling)
    {
        return;
    }
    _progressInfo = output;
    labelProgress->setText(_progressInfo);
}

//脚本执行时添加进度状态信息
void KyWindow::slotAppendProgressInfo(QString output)
{
    if(!_isInstalling)
    {
        return;
    }
    labelProgress->setText(_progressInfo+output);
}

//显示脚本执行时的进度百分值
void KyWindow::slotProgressValue(int value)
{
    if(!_isInstalling)
    {
        return;
    }
    if(value> 99)
    {
        //        textInstall->append("\nInstall Finished");
        //        labelProgress->setText("Install Finished");
    }
    else if(value > -1)
    {
        barProgress->setValue(value);
    }
    else
    {
        textInstall->insertPlainText("\nInstall Failed");
        labelProgress->setText(tr("Install Failed"));
    }
}

//在反馈区显示相关信息例如更新、进度
void KyWindow::slotOtherMsg(QString output)
{
    if(output == MSG_UPDATE_AI_FRAME)
    {
        _settings->setValue(SET_FRAME_XML_VERSION, _netFrameVersion);
        parseFrameXML();
        updateFrame(0);
        textInstall->append("View of AI Frame Installer Updated");
    }
    else if(output == MSG_UPDATE_AI_ASST1)
    {
        _settings->setValue(SET_AI_ASST1_VERSION, _netAsst1Version);
        textInstall->append("AI Frame Installer Updated");
    }
    else if(output == MSG_UPDATE_AI_ASST2)
    {
        _settings->setValue(SET_AI_ASST2_VERSION, _netAsst2Version);
        textInstall->append("AI Frame Installer Updated");
    }
    else if(output == MSG_PROG_FREE_START)
    {
        _progressValue = 0;
        _timer->start(200);
    }
    else if(output == MSG_PROG_FREE_END)
    {
        if(_progressValue<97)
        {
            _progressValue = 97+rand()%3;// <99
        }
        barProgress->setValue(_progressValue);
        _timer->stop();
    }
}


//执行脚本时在反馈区显示相关信息
void KyWindow::slotExecStandOutput()
{
    QString output = _exec->readAllStandardOutput();
    int lastLN = output.lastIndexOf('\n');
    if(lastLN == -1)
    {
        _standStr.append(output.mid(lastLN+1,-1));
    }
    else if(lastLN == output.size()-1)
    {
        slotStandOutput(_standStr+output.mid(0, lastLN));
        _standStr = "";
    }
    else
    {
        slotStandOutput(_standStr+output.mid(0, lastLN));
        _standStr = output.mid(lastLN+1,-1);
    }
}

//执行脚本时在反馈区显示相关错误信息
void KyWindow::slotExecStandError()
{
    QString output = _exec->readAllStandardError();
    int lastLN = output.lastIndexOf('\n');
    QString readyStr;
    if(lastLN == -1)
    {
        _errorStr.append(output.mid(lastLN+1,-1));
        return;
    }
    else if(lastLN == output.size()-1)
    {
        readyStr = _errorStr+output.mid(0, lastLN);
        _errorStr = "";
    }
    else
    {
        readyStr = _errorStr+output.mid(0, lastLN);
        _errorStr = output.mid(lastLN+1,-1);
    }
    QStringList strList = readyStr.split("\n");
    QString keep;
    foreach(QString str, strList)
    {
        if(str.startsWith("progIV:"))
        {
            if(str.at(7).toLatin1() == ' ')
            {
                slotProgressInfo(str.mid(8, -1));
            }
            else if(str.mid(7,-1) == "ProgFreeStart")
            {
                _execProgValue = 99;
                slotOtherMsg(MSG_PROG_FREE_START);
            }
            else if(str.mid(7,-1) == "ProgFreeEnd")
            {
                _execProgValue = 99;
                slotOtherMsg(MSG_PROG_FREE_END);
            }
            else if(str.at(7).toLatin1() == '=')//放弃使用，暂保留
            {

                slotProgressValue(str.mid(8, -1).toInt());
            }

        }
        else if(str.contains("......... .......... .......... .......... .........."))
        {
            _space++;
            if(_space%12!=0)
            {
                continue;
            }
            _space = 0;
            QRegExp reg;
            QString progValue = "  ";
            reg.setPattern("(\\d+)%");
            int index = reg.indexIn(str);

            if(index == -1)
            {
                continue;
            }
            int len = reg.matchedLength();
            int tValue = reg.cap(1).toInt();
            if(tValue != _execProgValue)
            {
                _execProgValue = tValue;
                slotProgressValue(_execProgValue);
            }

            //wget dot 方式 速度显示不准确
            reg.setPattern("([\\.\\d]+)([KMG ])");
            index = reg.indexIn(str, index+len);
            if(index == -1)
            {
                continue;
            }
            len = reg.matchedLength();
            //            progValue.append(reg.cap(1));
            //            char ch = str.at(index+len-1).toLatin1();
            //            switch (ch)
            //            {
            //            case 'K':
            //                progValue.append("KB/s");
            //                break;
            //            case 'M':
            //                progValue.append("MB/s");
            //                break;
            //            case 'G':
            //                progValue.append("GB/s");
            //                break;
            //            case ' ':
            //                progValue.append("B/s");
            //                break;
            //            }
            progValue.append(str.mid(index+len,-1));
            slotAppendProgressInfo(progValue);
        }
        else
        {
            keep.append(str);
            keep.append("\n");
        }
    }
    if(!keep.isEmpty())
    {
        slotErrorOutput(keep.remove(keep.size()-1,1));
    }
}

//开始执行脚本时在的状态设定
void KyWindow::slotExecStarted()
{
    slotStart(true);
}

//结束执行脚本时在的相关处理
void KyWindow::slotExecFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    slotFinish(exitCode, exitStatus, false);
}

//子进程执行相关命令
void KyWindow::exec(QString cmd)
{

    stopExec();
    _exec = new QProcess(this);

    connect(_exec, SIGNAL(started()), this, SLOT(slotExecStarted()));
    connect(_exec, SIGNAL(readyReadStandardOutput()), this, SLOT(slotExecStandOutput()));
    connect(_exec, SIGNAL(readyReadStandardError()), this, SLOT(slotExecStandError()));
    connect(_exec, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(slotExecFinished(int, QProcess::ExitStatus)));
    _exec->start(cmd);
}

//取消子进程正在执行的命令
void KyWindow::stopExec()
{
    if (_exec) {
        if (_exec->state() == QProcess::Running) {
            disconnect(_exec);
            _exec->close();
        }
        delete _exec;
        _exec = nullptr;
    }
}
