# AI Frame Installer

#### Description
The AI Framework Installer helps AI developers achieve one click installation of AI frameworks, allowing them to quickly invest in algorithm design and AI software development, eliminating the complex and error prone installation process.  

Supported installation of mainstream frameworks: PaddlePaddle, PyTorch, TensorFlow  
Supported versions: latest stable version, development version  
Supported installation methods: pip, conda  
Supported installation platforms: CUDA12.1, CUDA12.0, CUDA11.8, CUDA11.7, CPU  

The AI framework installation assistant has the following characteristics:  

*   Friendly interface, using familiar and intuitive AI framework options, and providing good installation process feedback.  
*   Intelligent recommendation, users only need to choose the AI framework they need to install, and the application intelligently recommends the best installation method based on the system hardware, while supporting user selection.  
*   One click automatic, software automatically installs the most suitable hardware drivers, acceleration tools, software dependencies, and configures the system environment. Eliminates the complex installation process and system environment configuration.  
*   No need to be on duty. If installation fails due to network or other reasons, it can automatically resume installation.  
*   The process is visible, and the software displays information on the various steps of installation, as well as the progress of each step.  
*   Breakpoints continue, supporting resource breakpoints to continue downloading, process breakpoints to continue installation, saving resources.  
*   Support hot updates to ensure timely updates of AI framework versions and installation methods, while also resolving potential software bugs as quickly as possible.  

We will continue to develop and optimize AI Frame Installer, support more mainstream AI frameworks and their suites, and keep up with the trend of artificial intelligence technology development to better assist AI developers!  

#### Software Architecture
Adopting a front-end and back-end architecture, front-end processing interface logic, detecting system hardware, automatically recommending installation methods, providing installation options, calling back-end installation interfaces, displaying installation information and progress; The backend adopts a script execution method to execute the task objectives passed by the frontend and provide feedback information.


#### Installation

1.  Run the script tools/install.sh
2.  Run the script tools/make deb. sh, then run `sudo dpkg -i 'generated deb file'` on the terminal
3.  Run `sudo apt install ai frame installer` on the terminal
4.  Install through the software store

#### Instructions

1. Run the software;
2. Select the AI framework that needs to be installed (the installation version, installation method, and computing platform can be changed as needed);
3. Click button Install and wait for the installation to complete.


#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
