# AI框架安装助手

#### 介绍
AI框架安装助手助力AI开发者实现一键安装AI框架，让AI开发者能够更快的投入到算法设计和AI软件开发，免去繁复易错的安装过程。  

支持安装主流框架：PaddlePadddle、PyTorch、TensorFlow  
支持版本：最新稳定版、开发版  
支持安装方式：pip、conda  
支持安装平台：CUDA12.1、CUDA12.0、CUDA11.8、CUDA11.7、CPU  

AI框架安装助手具有以下特点： 

*   界面友好，采用熟知、直观的AI框架选项，并提供良好的安装过程反馈。  
*   智能推荐，用户只需选择需要安装的AI框架，应用根据系统硬件智能推荐最佳安装方式，同时支持用户选择。  
*   一键自动，软件自动安装最适合的硬件驱动，加速工具，依赖软件，并且配置好系统环境。免去了繁复的安装过程和系统环境配置。  
*   无需值守，由于网络等原因造成的安装失败，能自动恢复继续安装。  
*   过程可见，软件显示安装的各个步骤，以及各个步骤的进度等信息。  
*   断点继续，支持资源断点继续下载，过程断点继续安装，节省资源。  
*   支持热更新，确保能及时更新AI框架版本以及安装方式，同时能以最快的速度解决软件可能存在的BUG。  

后续将持续开发，优化AI框架安装助手，支持更多主流AI框架及其套件等等，紧跟人工智能技术发展的潮流，更好地助力AI开发者！  

#### 软件架构
采用前后端架构方式，前端处理界面逻辑，检测系统硬件、自动推荐安装方式、提供安装选项、调用后端安装接口，显示安装信息及进度；后端采用脚本执行方式，执行前端传递的任务目标并反馈信息。


#### 安装教程

1.	运行脚本tools/install.sh
2.  运行脚本tools/make-deb.sh，然后在终端运行`sudo dpkg -i '生成的deb文件'`
3.  在终端运行`sudo apt install ai-frame-installer`
4.	通过软件商店安装

#### 使用说明

1.  运行软件；
2.  选择需要安装的AI框架（根据需要可以更改安装版本、安装方式、计算平台）；
3.  点击安装按钮，等待安装完成。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
