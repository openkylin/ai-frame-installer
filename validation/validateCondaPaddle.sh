#!/usr/bin/bash
echo "begin validate conda Paddle..."
source ~/anaconda3/etc/profile.d/conda.sh
conda env list
envs=($(conda env list | grep -E -o ^paddle_\\S+))
for env in ${envs[@]}
do
	echo "validate conda env $env"
	conda activate paddle_env && python -c '''
import platform
print(platform.architecture()[0])
print(platform.machine())
print()
import paddle
print(paddle.version.show())
paddle.utils.run_check()
'''
	conda deactivate
	echo
done
echo "validation of conda Paddle finished!"
read -n1 -s -p "Press any key to quit..."
echo
