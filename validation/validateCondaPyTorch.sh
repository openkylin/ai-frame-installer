#!/usr/bin/bash
echo "begin validate conda PyTorch..."
source ~/anaconda3/etc/profile.d/conda.sh
conda env list
envs=($(conda env list | grep -E -o ^torch_\\S+))
for env in ${envs[@]}
do
	echo "validate conda env $env"
	conda activate $env && python -c '''
import platform
print(platform.architecture()[0])
print(platform.machine())
print()
import torch
print("torch version:", torch.__version__)
print("gpu", torch.cuda.is_available())
x = torch.rand(5, 3)
print(x)
if torch.cuda.is_available():
    x = x.to("cuda")
    print(x)
else:
    print("CUDA is not available")
''' 
	conda deactivate
	echo
done

echo "validation of conda PyTorch finished!"
read -n1 -s -p "Press any key to quit..."
echo
