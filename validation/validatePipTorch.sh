#!/usr/bin/bash
python3.11 -c '''
import platform
print(platform.architecture()[0])
print(platform.machine())
print()
import torch
print("torch version:", torch.__version__)
print("cuda: ", torch.cuda.is_available())
x = torch.rand(5, 3)
print(x)
if torch.cuda.is_available():
    x = x.to("cuda")
    print(x)
else:
    print("CUDA is not available")
'''
read -n1 -s -p "Press any key to quit..."
echo
