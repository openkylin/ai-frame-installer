#!/usr/bin/bash
python3.11 -c '''
import platform
print(platform.architecture()[0])
print(platform.machine())
print()
import tensorflow as tf
print()
print("tensorflow version: ", tf.__version__)
print(tf.reduce_sum(tf.random.normal([1000, 1000])))
'''
read -n1 -s -p "Press any key to quit..."
echo
