#ifndef AIAssistant_H
#define AIAssistant_H

#include <QObject>
#include <QProcess>

void outLog(QString str);

class AIAssistant : public QObject
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "com.kylin.aiassistant")
public:
    AIAssistant();
    ~AIAssistant();


Q_SIGNALS:
    //执行脚本过程中发送的相关信号
    void sigExecStarted(bool);
    void sigExecFinished(int, int);

    void sigStandardOutput(QString);
    void sigStandardError(QString);
    void sigProgressInfo(QString);
    void sigAppendProgressInfo(QString);
    void sigProgressValue(int);

    void sigOtherMsg(QString);

public Q_SLOTS:
    //提供给其他app调用的总线接口
    void tempExec(QString cmd);
    void exec(QString cmd);
    void stopExec();
    void killAIAssistant();

//private slots:
    void slotStandOutput();
    void slotStandError();
    void slotExecStarted();
    void slotExecFinished(int, QProcess::ExitStatus);

private:
    //处理进程执行，输出，以及进度等信息的私有变量
    QProcess *_exec = nullptr;
    QString _standStr;
    QString _errorStr;

    int _space = -1;
    int _progValue = 0;
    bool _isProgFree = true;

};

#endif  // !AIAssistant_H
