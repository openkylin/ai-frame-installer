
#include <QString>
#include <QUrl>
#include <QDebug>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <QFile>
#include <QThread>
#include <QDir>
#include <QCoreApplication>
#include <sys/syslog.h>

#include "aiassistant.h"
#include "../src/pubdef.h"

//输出log信息
void outLog(QString str)
{
    str.prepend(QString("AIAssistant: "));
    syslog(LOG_INFO, "%s\n", str.toStdString().c_str());
}

AIAssistant::AIAssistant()
{
}



//析构类，停止运行正在运行的子进程，恢复暂时移除的程序，保持原系统完整性
AIAssistant::~AIAssistant()
{
    stopExec();
    tempExec("mv -f /usr/bin/kwalletd5_bak /usr/bin/kwalletd5");//恢复暂时移除的程序，保持原系统完整性
}

//创建临时子进程执行命令
void AIAssistant::tempExec(QString cmd)
{
    outLog(QString("tempExec") + QString(cmd));
    QProcess *process = new QProcess(this);
    connect(process, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
            [=](int exitCode, QProcess::ExitStatus exitStatus){
        if(cmd.contains("md5sum aiframe_bak.xml") && exitCode == 0 && exitStatus == 0)
        {
            Q_EMIT sigOtherMsg(MSG_UPDATE_AI_FRAME);
        }
        else if(cmd.contains("md5sum ai-asst1_bak") && exitCode == 0 && exitStatus == 0)
        {
            Q_EMIT sigOtherMsg(MSG_UPDATE_AI_ASST1);
        }
        else if(cmd.contains("md5sum ai-asst2_bak") && exitCode == 0 && exitStatus == 0)
        {
            Q_EMIT sigOtherMsg(MSG_UPDATE_AI_ASST2);
        }
        process->deleteLater();
    });
    process->start(cmd);
    process->waitForFinished();
}

//子进程执行过程的标准输出，处理并转发相关信号
void AIAssistant::slotStandOutput()
{
    QString output = _exec->readAllStandardOutput();
    int lastLN = output.lastIndexOf('\n');
    if(lastLN == -1)
    {
        _standStr.append(output.mid(lastLN+1,-1));
    }
    else if(lastLN == output.size()-1)
    {
        Q_EMIT sigStandardOutput(_standStr+output.mid(0, lastLN));
        _standStr = "";
    }
    else
    {
        Q_EMIT sigStandardOutput(_standStr+output.mid(0, lastLN));
        _standStr = output.mid(lastLN+1,-1);
    }
}

//子进程执行过程的错误输出，处理并转发相关信号
void AIAssistant::slotStandError()
{
    QString output = _exec->readAllStandardError();
    int lastLN = output.lastIndexOf('\n');
    QString readyStr;
    if(lastLN == -1)
    {
        _errorStr.append(output.mid(lastLN+1,-1));
        return;
    }
    else if(lastLN == output.size()-1)
    {
        readyStr = _errorStr+output.mid(0, lastLN);
        _errorStr = "";
    }
    else
    {
        readyStr = _errorStr+output.mid(0, lastLN);
        _errorStr = output.mid(lastLN+1,-1);
    }
    QStringList strList = readyStr.split("\n");
    QString keep;
    foreach(QString str, strList)
    {
        if(str.startsWith("progIV:"))
        {
            if(str.at(7).toLatin1() == ' ')
            {
                Q_EMIT sigProgressInfo(str.mid(8, -1));
            }
            else if(str.mid(7,-1) == "ProgFreeStart")
            {
                _progValue = 99;
                Q_EMIT sigOtherMsg(MSG_PROG_FREE_START);
            }
            else if(str.mid(7,-1) == "ProgFreeEnd")
            {
                 _progValue = 99;
                Q_EMIT sigOtherMsg(MSG_PROG_FREE_END);
            }
            else if(str.at(7).toLatin1() == '=')//放弃使用，暂保留
            {

                Q_EMIT sigProgressValue(str.mid(8, -1).toInt());
            }

        }
        else if(str.contains("......... .......... .......... .......... .........."))
        {
            _space++;
            if(_space%12!=0)
            {
                continue;
            }
            _space = 0;
            QRegExp reg;
            QString progValue = "  ";
            reg.setPattern("(\\d+)%");
            int index = reg.indexIn(str);

            if(index == -1)
            {
                continue;
            }
            int len = reg.matchedLength();
            int tValue = reg.cap(1).toInt();
            if(tValue != _progValue)
            {
                _progValue = tValue;
                Q_EMIT sigProgressValue(_progValue);
            }

            //wget dot 方式 速度显示不准确
            reg.setPattern("([\\.\\d]+)([KMG ])");
            index = reg.indexIn(str, index+len);
            if(index == -1)
            {
                continue;
            }
            len = reg.matchedLength();
//            progValue.append(reg.cap(1));
//            char ch = str.at(index+len-1).toLatin1();
//            switch (ch)
//            {
//            case 'K':
//                progValue.append("KB/s");
//                break;
//            case 'M':
//                progValue.append("MB/s");
//                break;
//            case 'G':
//                progValue.append("GB/s");
//                break;
//            case ' ':
//                progValue.append("B/s");
//                break;
//            }
            progValue.append(str.mid(index+len,-1));
            Q_EMIT sigAppendProgressInfo(progValue);
        }
        else
        {
            keep.append(str);
            keep.append("\n");
        }
    }
    if(!keep.isEmpty())
    {
        Q_EMIT sigStandardError(keep.remove(keep.size()-1,1));
    }
}

//子进程执行开始，转发相关信号
void AIAssistant::slotExecStarted()
{
    Q_EMIT sigExecStarted(true);
}

//子进程执行结束，转发相关信号
void AIAssistant::slotExecFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    Q_EMIT sigExecFinished(exitCode, exitStatus);
}

//通过子进程执行命令
void AIAssistant::exec(QString cmd)
{
    outLog(QString("exec") + QString(cmd));
    stopExec();
    _exec = new QProcess(this);

    connect(_exec, SIGNAL(started()), this, SLOT(slotExecStarted()));
    connect(_exec, SIGNAL(readyReadStandardOutput()), this, SLOT(slotStandOutput()));
    connect(_exec, SIGNAL(readyReadStandardError()), this, SLOT(slotStandError()));
    connect(_exec, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(slotExecFinished(int, QProcess::ExitStatus)));
    _exec->start(cmd);
}

//停止正在执行的子进程
void AIAssistant::stopExec()
{
    if (_exec) {
        outLog("stop exec");
        if (_exec->state() == QProcess::Running) {
            disconnect(_exec);
            _exec->close();
        }
        delete _exec;
        _exec = nullptr;
    }
}


//终止程序
void AIAssistant::killAIAssistant()
{
    outLog("kill AIAssistant!");
    stopExec();
    QCoreApplication::quit();
}
