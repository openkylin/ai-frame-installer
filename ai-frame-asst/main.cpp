#include <QCoreApplication>
#include <QDBusConnection>
#include <QDBusError>
#include <QDebug>

#include "aiassistant.h"

int main(int argc, char *argv[])
{
    //应用名设置
    QCoreApplication app(argc, argv);
    app.setOrganizationName("kylin aiassistant");
    app.setApplicationName("kylin aiassistant");

    //注册总线，设置相关接口
    QDBusConnection systemBus = QDBusConnection::systemBus();
    if (!systemBus.isConnected()) {
        outLog("d-bus connection fail !");
        printf("d-bus connection fail !");
        return -1;
    }

    if (!systemBus.registerService("com.kylin.aiassistant")) {
        outLog(QString("d-bus register service fail ! d-bus error : ") +
               QDBusError::errorString(systemBus.lastError().type()));
        printf("d-bus register service fail ! d-bus error!");
        return -1;
    }

    //设置总线服务类
    if (!systemBus.registerObject("/", new AIAssistant,
                                  QDBusConnection::ExportAllSlots | QDBusConnection::ExportAllSignals)) {
        outLog(QString("d-bus register object fail ! d-bus error : ") +
               QDBusError::errorString(systemBus.lastError().type()));
        printf("d-bus register object fail ! d-bus error !");
        return -1;
    }

    return app.exec();
}
